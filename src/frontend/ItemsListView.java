package frontend;

import backend.*;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 *Extended listView, which refreshes itself automatically and handles mouse events.
 * It also handles displaying list item's info in a VBox passed to the constructor,
 * and operations such as removal, and showing more specific data, for example plots.
 *
 */
public class ItemsListView extends ListView {
    private VBox itemInfoPanel;
    private Supplier actualItems;
    private Button deleteButton;
    private ObservableList observableList;
    private List filteredList;
    private boolean disableDelete = false;


    /**
     * @param actualItems - includes a reference to the method returning the up-to-date List of items.
     * @param itemInfoPanel - reference to VBox, in which specific info about list's items should be shown.
     * @param <T>
     */
    public <T extends Item> ItemsListView(Supplier<List<T>> actualItems, VBox itemInfoPanel) {
        super();
        this.actualItems = actualItems;
        this.itemInfoPanel = itemInfoPanel;

        this.observableList = FXCollections.observableList(actualItems.get());
        this.setItems(this.observableList);
        this.setCellFactory(listView1 -> {
            TextFieldListCell<T> cell = new TextFieldListCell<>();
            cell.setConverter(new StringConverter<T>() {
                @Override
                public String toString(T item) {
                    return item.getName();
                }

                @Override
                public T fromString(String s) {
                    return null;
                }
            });
            return cell;
        });

        this.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener() {
                    @Override
                    public void changed(ObservableValue observableValue, Object o, Object t1) {
                        showSelectedItemInfo();
                    }
                });
        super.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                showSelectedItemInfo();
            }
        });
    }

    /**
     * Displaying item's info in itemInfoPanel, triggered on click.
     */
    private void showSelectedItemInfo() {
        final Item item = (Item) super.getSelectionModel().getSelectedItem();
        GridPane gridPane = null;
        if(item != null) {
            itemInfoPanel.getChildren().clear();

            gridPane = new GridPane();
            int column = 0;
            int row = 0;
            for (Map.Entry<String, String> entry : item.getShowableInfo().entrySet()) {
                gridPane.add(new Label(entry.getKey() + ":"), column++, row);
                gridPane.add(new Label(entry.getValue()), column--, row++);
            }
            gridPane.getStyleClass().add("itemInfoLayout");

            Image image = new Image(this.getClass().getResource(item.getImgPath()).toString());
            double aspectRatio = image.getHeight() / image.getWidth();
            ImageView itemImageView = new ImageView(image);

            if (image.getWidth() > image.getHeight()) {
                itemImageView.setFitWidth(200);
                itemImageView.setFitHeight(200 * aspectRatio);
            } else {
                itemImageView.setFitHeight(200);
                itemImageView.setFitWidth(200 / aspectRatio);
            }
            StackPane imageStackPane = new StackPane();
            imageStackPane.getChildren().add(itemImageView);
            itemInfoPanel.getChildren().add(imageStackPane);
            itemInfoPanel.getChildren().add(gridPane);

            deleteButton = new Button("USUŃ!");
            if(Simulator.isRunning() || disableDelete) {
                deleteButton.setDisable(true);
            }
            deleteButton.setOnAction(actionEvent -> {
                if(Simulator.isRunning() == false) {
                    Item element = (Item) ItemsListView.this.getSelectionModel().getSelectedItem();
                    element.handleRemoval();
                    VodManager.removeItem(element);
                    this.reloadList();
                    itemInfoPanel.getChildren().clear();
                }
            });

            HBox buttonsHBox = new HBox();
            buttonsHBox.getChildren().add(deleteButton);

            if(item instanceof Product) {
                Button showPlotButton = new Button("WYKRES");
                showPlotButton.setOnAction(e -> {
                    showPlot(((Product) item).getViewsHistory());
                });
                Button showDescriptionButton = new Button("RECENZJA");
                showDescriptionButton.setOnAction(actionEvent -> {
                    showDescription(((Product) item).getDescription());
                });
                buttonsHBox.getChildren().addAll(showPlotButton, showDescriptionButton);
            }

            itemInfoPanel.getChildren().add(buttonsHBox);
        }
    }

    /**
     *Hides visible item's info, used for example after item's removal.
     */
    public void hideSelectedItemInfo() {
        itemInfoPanel.getChildren().clear();
    }

    /**
     *Disables removeButton. Used to prevent user from deleting items while simulation is running
     * in order to prevent unpredictable behaviour.
     */
    public void disableRemoveButton() {
        if(deleteButton != null) {
            deleteButton.setDisable(true);
        }
    }

    /**
     *Enables removeButton.
     */
    public void enableRemoveButton() {
        if(deleteButton != null) {
            deleteButton.setDisable(false);
        }
    }

    /**
     * Reloads list. Used after mutating list's content and once a day during simulation.
     * @param predicate - it defines filter, which could be set on the list.
     */
    public void reloadList(Predicate predicate) {
        hideSelectedItemInfo();
        disableDelete = true;
        filteredList = (List) ((List) actualItems.get()).stream().filter(predicate)
                .collect(Collectors.toList());
        observableList = FXCollections.observableList(filteredList);
        this.setItems(observableList);
        this.refresh();
    }

    /**
     *Reloads list. Used after mutating list's content and once a day during simulation.
     * Always all items visible, no filter.
     */
    public void reloadList() {
        hideSelectedItemInfo();
        disableDelete = false;
        filteredList = null;
        observableList = FXCollections.observableList((List) actualItems.get());
        this.setItems(observableList);
        this.refresh();
    }

    /**
     * Creates a new window with a plot.
     * @param viewsHistory
     */
    public void showPlot(List viewsHistory) {
        Stage stage = new Stage();

        NumberAxis xAxis = new NumberAxis();
        NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Dzień symulacji");

        final LineChart<Number,Number> lineChart =
                new LineChart<>(xAxis,yAxis);

        lineChart.setTitle("Wyświetlenia");

        XYChart.Series series = new XYChart.Series();
        series.setName("Wyświetlenia");

        for(int x = 0; x < viewsHistory.size()-1; x++) {
            series.getData().add(new XYChart.Data(x, viewsHistory.get(x+1)));
        }

        lineChart.getData().add(series);
        Scene scene  = new Scene(lineChart,800, 600);
        stage.setScene(scene);

        stage.show();
    }

    /**
     * Creates a new window with product's description.
     * @param description
     */
    public void showDescription(String description) {
        Stage stage = new Stage();
        StackPane stackPane = new StackPane();
        stackPane.setPadding(new Insets(20));
        Label text = new Label(description);
        text.setFont(new Font(20));
        text.setWrapText(true);
        stackPane.getChildren().add(text);

        Scene scene  = new Scene(stackPane,800,200);

        stage.setScene(scene);
        stage.show();
    }
}
