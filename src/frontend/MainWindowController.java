package frontend;

import backend.*;
import backend.exceptions.NoProvidersException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class MainWindowController implements Initializable {
    public BorderPane mainBorderPane;
    public VBox leftBorder;
    public VBox rightBorder;
    public TabPane tabPane;
    private Tab seriesTab;
    private Tab filmsTab;
    private Tab streamsTab;
    private Tab usersTab;
    private Tab providersTab;
    public Label daysPassedLabel;

    private ItemsListView seriesListView;
    private ItemsListView filmsListView;
    private ItemsListView streamsListView;
    private ItemsListView usersListView;
    private ItemsListView providersListView;

    public Button startStopSimulationButton;
    public ButtonBar topBorder;
    public Button filterItemsButton;
    public Button clearFilterButton;
    public Button saveButton;
    public Button loadButton;
    public Label budgetLabel;

    public TextField filterItemsTextField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Simulator.setMainWindowController(this);

        seriesListView = new ItemsListView(() -> {
            return VodManager.getSeries();
        }, rightBorder);
        filmsListView = new ItemsListView(() -> {
            return VodManager.getFilms();
        }, rightBorder);
        streamsListView = new ItemsListView(() -> {
            return VodManager.getStreams();
        }, rightBorder);
        usersListView = new ItemsListView(() -> {
            return VodManager.getUsers();
        }, rightBorder);
        providersListView = new ItemsListView(() -> {
            return VodManager.getProviders();
        }, rightBorder);

        seriesTab = new Tab("SERIALE", seriesListView);
        filmsTab = new Tab("FILMY", filmsListView);
        streamsTab = new Tab("STREAMY", streamsListView);
        usersTab = new Tab("KLIENCI", usersListView);
        providersTab = new Tab("DYSTRYBUTORZY", providersListView);
        tabPane.getTabs().addAll(seriesTab, filmsTab, streamsTab, usersTab, providersTab);
        tabPane.getSelectionModel().selectedItemProperty().addListener(e -> {
            rightBorder.getChildren().clear();
            ((ItemsListView) tabPane.getSelectionModel().getSelectedItem().getContent()).reloadList();
        });
        tabPane.setPadding(new Insets(20));

        rightBorder.setPrefWidth(400);
        filterItemsButton.setGraphic(new ImageView(new Image((this.getClass().getResource("pictures/search.png").toString()),
                20, 20, true, true)));
        clearFilterButton.setGraphic(new ImageView(new Image((this.getClass().getResource("pictures/clear.png").toString()),
                20, 20, true, true)));

        updateBudgetLabel(VodManager.getBudget());


    }

    public void addNewSeries(ActionEvent actionEvent) {
        try {
            Series series = Faker.getSeries();
            VodManager.addProduct(series);
            updateVisibleList();
        } catch (NoProvidersException e) {
            alert("Musisz najpierw dodać jakichś dystrubutorów!");
        }
    }

    public void addNewFilm(ActionEvent actionEvent) {
        try {
            Film film = Faker.getFilm();
            VodManager.addProduct(film);
            updateVisibleList();
        } catch(NoProvidersException e) {
            alert("Musisz najpierw dodać jakichś dystrubutorów!");
        }

    }

    public void addNewStream(ActionEvent actionEvent) {
        try {
            Stream stream = Faker.getStream();
            VodManager.addProduct(stream);
            updateVisibleList();
        } catch(NoProvidersException e) {
            alert("Musisz najpierw dodać jakichś dystrubutorów!");
        }
    }

    public void addNewProvider(ActionEvent actionEvent) {
        Provider provider = Faker.getProvider();
        VodManager.addProvider(provider);
        updateVisibleList();
    }

    public void addNewUser(ActionEvent actionEvent) {
        User user = Faker.getUser();
        VodManager.addUser(user);
        updateVisibleList();
    }

    public void startStopSimulation(ActionEvent actionEvent) {
        if (Simulator.isRunning() == false) {
            Simulator.start();
            startStopSimulationButton.setText("STOP");
        } else {
            Simulator.stop();
            startStopSimulationButton.setText("START");
        }
    }

    public void updateDaysPassed(Integer daysPassedValue) {
        Platform.runLater(() -> daysPassedLabel.setText("DZIEŃ " + daysPassedValue.toString()));
    }

    public void updateBudgetLabel(float budget) {
        Platform.runLater(() -> budgetLabel.setText("Budżet: " + Float.toString(budget)));
    }

    public void updateVisibleList() {
        Platform.runLater(() -> {
            ((ItemsListView) tabPane.getSelectionModel().getSelectedItem().getContent()).reloadList();
        });
    }

    public void disableButtons() {
        Platform.runLater(() -> {
            ((ItemsListView) tabPane.getSelectionModel().getSelectedItem().getContent()).disableRemoveButton();
            filterItemsButton.setDisable(true);
            clearFilterButton.setDisable(true);
            saveButton.setDisable(true);
            loadButton.setDisable(true);
        });
    }

    public void enableButtons() {
        Platform.runLater(() -> {
            ((ItemsListView) tabPane.getSelectionModel().getSelectedItem().getContent()).enableRemoveButton();
            filterItemsButton.setDisable(false);
            clearFilterButton.setDisable(false);
            saveButton.setDisable(false);
            loadButton.setDisable(false);
        });
    }

    public void filterItems(ActionEvent actionEvent) {
        if(filterItemsTextField.getText().isEmpty() == false) {
            ((ItemsListView) tabPane.getSelectionModel().getSelectedItem().getContent())
                    .reloadList(new Predicate() {
                        @Override
                        public boolean test(Object o) {
                            return ((Item) o).matches(filterItemsTextField.getText());
                        }
                    });
        }
    }

    public void changePrices(ActionEvent actionEvent) {
        Stage stage = new Stage();
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10));

        gridPane.add(new Label("ODCINEK SERIALU: "), 0, 0);
        gridPane.add(new Label("FILM: "), 0, 1);
        gridPane.add(new Label("STREAM: "), 0, 2);
        gridPane.add(new Label("ABONAMENT BASIC: "), 0, 3);
        gridPane.add(new Label("ABONAMENT FAMILY: "), 0, 4);
        gridPane.add(new Label("ABONAMENT PREMIUM: "), 0, 5);

        TextField series, film, stream, basic, family, premium;
        series = new TextField(Config.getSeriesDefaultPrice().toString());
        film = new TextField(Config.getFilmDefaultPrice().toString());
        stream = new TextField(Config.getStreamDefaultPrice().toString());
        basic = new TextField(Config.getDefaultBasicPrice().toString());
        family = new TextField(Config.getDefaultFamilyPrice().toString());
        premium= new TextField(Config.getDefaultPremiumPrice().toString());

        gridPane.add(series, 1, 0);
        gridPane.add(film, 1, 1);
        gridPane.add(stream, 1, 2);
        gridPane.add(basic, 1, 3);
        gridPane.add(family, 1, 4);
        gridPane.add(premium, 1, 5);

        Button save = new Button("ZAPISZ");
        save.setOnAction(e -> {
            boolean success = true;
            Float seriesPrice = null, filmPrice = null, streamPrice = null,
                    basicPrice = null, familyPrice = null, premiumPrice = null;
            try {
                seriesPrice = Float.parseFloat(series.getText());
                filmPrice = Float.parseFloat(series.getText());
                streamPrice = Float.parseFloat(series.getText());
                basicPrice = Float.parseFloat(series.getText());
                familyPrice = Float.parseFloat(series.getText());
                premiumPrice = Float.parseFloat(series.getText());
            } catch(NumberFormatException exception) {
                success = false;
            }
            if(success) {
                Config.setDefaultBasicPrice(basicPrice);
                Config.setDefaultFamilyPrice(familyPrice);
                Config.setDefaultPremiumPrice(premiumPrice);
                Config.setSeriesDefaultPrice(seriesPrice);
                Config.setFilmDefaultPrice(filmPrice);
                Config.setStreamDefaultPrice(streamPrice);
                stage.close();
            }
        });
        Button cancel = new Button("ANULUJ");
        cancel.setOnAction(e -> {
            stage.close();
        });
        gridPane.add(save, 0, 6);
        gridPane.add(cancel, 1, 6);

        Scene scene  = new Scene(gridPane,400,200);
        stage.setScene(scene);
        stage.showAndWait();

        System.out.println(Config.getFilmDefaultPrice());
        System.out.println(Config.getSeriesDefaultPrice());
        System.out.println(Config.getStreamDefaultPrice());

        System.out.println(Config.getDefaultBasicPrice());
        System.out.println(Config.getDefaultPremiumPrice());
        System.out.println(Config.getDefaultFamilyPrice());

    }

    public void clearFilter(ActionEvent actionEvent) {
        ((ItemsListView) tabPane.getSelectionModel().getSelectedItem().getContent()).reloadList();
    }

    public void alert(String s) {
        new SimpleAlert(s);
    }

    public void loadSimulationState(ActionEvent actionEvent) {
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new BufferedInputStream(
                            new FileInputStream(Config.getSerializedDataPath())));
            Config.loadSerializedObjects((HashMap<ConfigField, Object>) in.readObject());
            Simulator.loadSerializedObjects((HashMap<SimulatorField, Object>) in.readObject());
            VodManager.loadSerializedObjects((HashMap<VodManagerField, Object>) in.readObject());
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        ((ItemsListView) tabPane.getSelectionModel().getSelectedItem().getContent()).reloadList();
        updateBudgetLabel(VodManager.getBudget());
        updateDaysPassed(Simulator.getDaysPassed());
    }

    public void saveSimulationState(ActionEvent actionEvent) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new BufferedOutputStream(
                            new FileOutputStream(Config.getSerializedDataPath())));
            out.writeObject(Config.getObjectsToSerialize());
            out.writeObject(Simulator.getObjectsToSerialize());
            out.writeObject(VodManager.getObjectsToSerialize());
            out.close();
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void handleBankruptcy() {
        Platform.runLater(() -> {
            startStopSimulationButton.setText("START");
            alert("BANKRUCTWO :(");
        });
    }
}