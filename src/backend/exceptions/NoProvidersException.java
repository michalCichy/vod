package backend.exceptions;

public class NoProvidersException extends RuntimeException {
    public NoProvidersException() {
        super("NoProvidersException");
    }

    public NoProvidersException(String message) {
        super(message);
    }
}
