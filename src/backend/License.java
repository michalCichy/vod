package backend;

import java.io.Serializable;

public class License implements Serializable {
    private String name;
    private Float price;
    private int devicesLimit;
    private Quality maximalQuality;

    public License(String name, Float price, int devicesLimit, Quality maximalQuality) {
        this.name = name;
        this.price = price;
        this.devicesLimit = devicesLimit;
        this.maximalQuality = maximalQuality;
    }

    public String getName() {
        return name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) { this.price = price; }

    public int getDevicesLimit() {
        return devicesLimit;
    }

    public Quality getMaximalQuality() {
        return maximalQuality;
    }
}
