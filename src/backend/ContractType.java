package backend;

import java.io.Serializable;

public enum ContractType implements Serializable {
    PER_VIEW,
    PER_MONTH
}
