package backend;

import java.io.Serializable;
import java.util.Date;

public class LibraryItem implements Serializable {
    private Product product;
    private Date purchaseDate;
    private boolean watched = false;
    private boolean rated = false;

    public LibraryItem(Product product, Date purchaseDate) {
        this.product = product;
        this.purchaseDate = purchaseDate;
    }

    public void registerWatching(double vote) {
        product.registerView();
        watched = true;
        if(rated == false) {
            product.registerRating(vote);
        }
    }

    public void registerWatching() {
        product.registerView();
        watched = true;
    }

    public Product getProduct() {
        return product;
    }

    public boolean wasWatched() {
        return watched;
    }
}
