package backend;

import java.io.Serializable;
import java.util.Date;
import backend.exceptions.*;

public class Discount implements Serializable {
    private Date start;
    private Date end;
    private float percentage;

    public Discount(Date start, Date end, float percentage) {
        this.start = start;
        this.end = end;
        this.percentage = percentage;
    }
}
