package backend;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Series extends Product implements Serializable {
    private ArrayList<ArrayList<Episode>> episodesRegister;
    private CinematicGenre genre;
    public Series(
            String name,
            Float price,
            String imgPath,
            String description,
            Date worldReleaseDate,
            Date vodReleaseDate,
            Provider provider,
            ArrayList<String> cast,
            String productionCountry,
            CinematicGenre genre) {
        super(name, price, imgPath, description, worldReleaseDate, vodReleaseDate, provider, cast, productionCountry);
        this.genre = genre;
        this.episodesRegister = new ArrayList<>();
    }

    public ArrayList<ArrayList<Episode>> getEpisodesRegister() {
        return episodesRegister;
    }

    public CinematicGenre getGenre() {
        return genre;
    }

    @Override
    public int getDuration() {
        int sum = 0;
        for(ArrayList<Episode> season : episodesRegister) {
            for(Episode episode : season) {
                sum += episode.getDuration();
            }
        }
        return sum;
    }

    public int getEpisodesCount() {
        int sum = 0;
        for(ArrayList<Episode> season : episodesRegister) {
            sum += season.size();
        }
        return sum;
    }

    @Override
    public String getTextInfo() {
        String productInfo = super.getTextInfo();
        return "SERIES " + productInfo + (genre == null ? "BRAK" : genre);
    }

    @Override
    public HashMap<String, String> getShowableInfo() {
        HashMap<String, String> info = super.getShowableInfo();
        info.put("liczba sezonów", Integer.toString(episodesRegister.size()));
        info.put("gatunek", Product.convertCinematicGenreToString(genre));

        return info;
    }

    @Override
    public LibraryItem getLibraryItem() {
        return new SeriesLibraryItem(this, Simulator.getNow());
    }

    public synchronized void addEpisode() {
        if(episodesRegister.size() == 0) {
            episodesRegister.add(new ArrayList<>());
            episodesRegister.get(0).add(new Episode(Simulator.getNow(),
                    1 + (int)(Math.random()/2 * Config.getMaximalSingleProductDuration())));
        } else {
            if(Weglarz.probability(0.1)) {
                episodesRegister.add(new ArrayList<>());
            }
            episodesRegister.get(episodesRegister.size() - 1).add(new Episode(Simulator.getNow(),
                    1 + (int)(Math.random()/2 * Config.getMaximalSingleProductDuration())));
        }
    }
}
