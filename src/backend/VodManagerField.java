package backend;

public enum VodManagerField {
    SERIES,
    FILMS,
    STREAMS,
    USERS,
    PROVIDERS,
    BUDGET,
    LICENSES
}
