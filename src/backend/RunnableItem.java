package backend;

import java.io.Serializable;

import static java.lang.Thread.sleep;

public abstract class RunnableItem extends Item implements Serializable {
    private transient Thread thread;
    protected Logger logger;
    private boolean deleted = false;
    private int maximalSleepTime;

    public RunnableItem(int maximalSleepTime) {
        this.logger = new Logger(Config.isRunnableItemLogEnabled());
        this.maximalSleepTime = maximalSleepTime;
    }
    protected abstract void performRoutine();

    @Override
    public void handleRemoval() {
        deleted = true;
    }

    public void startThread() {
        if(thread != null && thread.isAlive()) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (Simulator.isRunning() == true && deleted == false) {
                        Simulator.getEndDaySemaphore().p();
                        Simulator.getEndDaySemaphore().v();
                        Simulator.getMainSemaphore().p();
                        performRoutine();
                        Simulator.getMainSemaphore().v();
                        sleep(maximalSleepTime);
                    }
                } catch(InterruptedException exception) {

                }
            }
        });
        thread.start();
    }

    public void stopThread() {
        thread.interrupt();
    }

    public boolean isRunning() {
        return thread.isAlive();
    }

    public void terminateThread() {
        deleted = true;
    }
}
