package backend;

import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.Serializable;
import java.util.*;

public class Product extends Item implements Serializable {
    private String name;
    private Float price;
    private String imgPath;
    private String description;
    private Date worldReleaseDate;
    private Date vodReleaseDate;
    private Provider provider;
    private Double rating;
    private int ratingCount;
    private ArrayList<String> cast;
    private String productionCountry;
    private int views;
    private volatile List viewsHistory;

    private static HashMap<CinematicGenre, String> cinematicGenreToString;
    private static HashMap<StreamGenre, String> streamGenreToString;

    static {
        cinematicGenreToString = new HashMap<>();
        cinematicGenreToString.put(CinematicGenre.ACTION, "sensacyjny");
        cinematicGenreToString.put(CinematicGenre.DRAMA, "dramat");
        cinematicGenreToString.put(CinematicGenre.KIDS, "dla dzieci");
        cinematicGenreToString.put(CinematicGenre.DOCUMENTARY, "dokumentalny");
        cinematicGenreToString.put(CinematicGenre.COMEDY, "komedia");

        streamGenreToString = new HashMap<>();
        streamGenreToString.put(StreamGenre.CONCERT, "koncert");
        streamGenreToString.put(StreamGenre.THEATRICAL_PERFORMANCE, "sztuka teatralna");
        streamGenreToString.put(StreamGenre.OTHER, "inne");
    }

    public Product(
            String name,
            Float price,
            String imgPath,
            String description,
            Date worldReleaseDate,
            Date vodReleaseDate,
            Provider provider,
            ArrayList<String> cast,
            String productionCountry) {
        this.name = name;
        this.price = price;
        this.imgPath = imgPath;
        this.description = description;
        this.worldReleaseDate = worldReleaseDate;
        this.vodReleaseDate = vodReleaseDate;
        this.provider = provider;
        this.cast = cast;
        this.productionCountry = productionCountry;
        this.views = 0;
        this.rating = 0d;
        this.ratingCount = 0;

        viewsHistory = Collections.synchronizedList(new ArrayList<Integer>());
        viewsHistory.add(0);
        viewsHistory.add(views);
    }

    public void registerNewDay() {
        viewsHistory.add(views);
    }

    public synchronized void registerView() {
        viewsHistory.set( viewsHistory.size()-1, ++views);

    }
    public synchronized void registerRating(double vote) {
        ratingCount++;
        rating = (rating * (ratingCount-1) + vote) / ratingCount;
    }

    public String getName() {
        return name;
    }

    public String getImgPath() {
        return imgPath;
    }

    @Override
    public void handleRemoval() {

    }

    public String getDescription() {
        return description;
    }

    public Date getWorldReleaseDate() {
        return worldReleaseDate;
    }

    public Date getVodReleaseDate() {
        return vodReleaseDate;
    }

    public int getDuration() {return 0;}

    public Provider getProvider() {
        return provider;
    }

    public double getRating() {
        return rating;
    }

    public ArrayList<String> getCast() {
        return cast;
    }

    public String getProductionCountry() {
        return productionCountry;
    }

    public Float getPrice() {
        return price;
    }

    public int getViews() {
        return views;
    }

    public List getViewsHistory() {
        return viewsHistory;
    }

    public String getTextInfo() {
        String info = "PRODUCT" +
                " " + (name == null ? "brak" : name) +
                " " + price +
                " " + (imgPath == null ? "brak" : imgPath) +
                " " + (description == null ? "brak" : description) +
                " " + (worldReleaseDate == null ? "brak" : worldReleaseDate) +
                " " + (vodReleaseDate == null ? "brak" : vodReleaseDate) +
                " " + (provider == null ? "brak" : provider.getName()) +
                " " + (rating == null ? "brak" : Double.toString(rating));
        if(cast != null)
        for(String actor : cast) {
            info += actor + " ";
        }
        info += "| " +
        Integer.toString(views);

        return info;
    }

    @Override
    public HashMap<String, String> getShowableInfo() {
        LinkedHashMap<String, String> info = new LinkedHashMap<>();
        info.put("nazwa", name);
        info.put("cena", Float.toString(price));
        info.put("premiera światowa", worldReleaseDate.toString());
        info.put("premiera w naszym vod", vodReleaseDate.toString());
        info.put("dystrybutor", provider.getName());
        info.put("ocena", rating.toString());
        String castString = "";
        for(String actor : this.cast) {
            castString += actor + ", ";
        }
        info.put("obsada", castString);
        info.put("kraj produkcji", productionCountry);
        info.put("liczba wyświetleń", Integer.toString(views));
        return info;
    }

    public static String convertCinematicGenreToString(CinematicGenre genre) {
        return cinematicGenreToString.get(genre);
    }

    public static String convertStreamGenreToString(StreamGenre genre) {
        return streamGenreToString.get(genre);
    }

    public LibraryItem getLibraryItem() {
        return new LibraryItem(this, Simulator.getNow());
    }

    public int getRecentViews() {
        return (Integer) viewsHistory.get(viewsHistory.size()-1) - (Integer) viewsHistory.get(viewsHistory.size()-2);
    }

    @Override
    public boolean matches(String searchText) {
        String searchTextLoweCase = searchText.toLowerCase();
        return searchText.isEmpty() ? true :  name.toLowerCase().contains(searchTextLoweCase) ||
                cast.stream().anyMatch(p -> {return p.toLowerCase().contains(searchTextLoweCase);});
    }
}
