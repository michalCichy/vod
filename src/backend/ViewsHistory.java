package backend;

import java.io.Serializable;
import java.util.ArrayList;

public class ViewsHistory extends ArrayList<Integer> implements Serializable {
    public ViewsHistory() {
        this.add(0);
        this.add(0);
    }

    public int getRecentViews() {
        return this.get(this.size()-1) - this.get(this.size()-2);
    }

    public void registerNewDay(int viewsAtStart) {
        this.add(viewsAtStart);
    }
}
