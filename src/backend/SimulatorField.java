package backend;

public enum SimulatorField {
    SIMULATION_STARTED,
    SIMULATION_ENDED,
    NOW,
    DAYS_PASSED,
    NEGATIVE_DAYS
}
