package backend;

import java.io.Serializable;

public class Logger implements Serializable {
    private boolean enabled;

    public Logger(boolean enabled) {
        this.enabled = enabled;
    }

    public void log(String logString) {
        if(enabled) {
            System.out.println(logString);
        }
    }

}
