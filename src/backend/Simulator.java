package backend;
import frontend.MainWindowController;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.*;

import static java.lang.Thread.sleep;

public class Simulator implements StaticSerializable {
    private static volatile boolean isRunning = false;
    public static volatile boolean simulationEnded = false;
    public static volatile boolean simulationStarted = false;
    private static Date now;
    private static ScheduledExecutorService timer;
    private static Integer daysPassed = 0;
    private static SimpleSemaphore mainSemaphore;
    private static SimpleSemaphore endDaySemaphore;
    private static Object threadsStopper;
    private static int negativeDays = 0;

    private static MainWindowController mainWindowController;

    static {
        now = new Date();
    }

    public static void setMainWindowController(MainWindowController mainWindowControllerReference) {
        mainWindowController = mainWindowControllerReference;
    }

    public static void initialize() {
        mainSemaphore = new SimpleSemaphore(Config.getSimulationMaximalThreadsCount());
        endDaySemaphore = new SimpleSemaphore(1);
        threadsStopper = new Object();
    }

    public static void start() {
        if(isRunning == false) {
            if(simulationStarted == false) {
                initialize();
                simulationStarted = true;
            }
            mainWindowController.disableButtons();
            VodManager.startThreads();
            isRunning = true;
            if(timer != null) {
                timer.shutdown();
            }
            timer = Executors.newSingleThreadScheduledExecutor();
            timer.scheduleAtFixedRate(() -> {
                if(isRunning == false) {
                    timer.shutdown();
                }
                try {
                    Simulator.endDay();
                } catch (InterruptedException e) {
                    System.out.println("Nie udalo sie zakonczyc dnia");
                    e.printStackTrace();
                }
            }, Config.getSimulationDayDuration(), Config.getSimulationDayDuration(), TimeUnit.MILLISECONDS);
        }
    }

    private static void incrementNow() {
        Calendar calendar = (Calendar.getInstance());
        calendar.setTime(now);
        calendar.add(Calendar.DATE, 1);
        now = calendar.getTime();
    }

    private static void sumUpDay() {
        daysPassed++;
        VodManager.registerNewDay();
        VodManager.manageDailyBudget();
        VodManager.updateLicenses();
        if(daysPassed%30 == 0) {
            VodManager.manageMonthlyBudget();
        }
        mainWindowController.updateDaysPassed(daysPassed);
        mainWindowController.updateBudgetLabel(VodManager.getBudget());
        mainWindowController.updateVisibleList();
        incrementNow();

        if(VodManager.negativeBudget() == true) {
            negativeDays++;
        } else {
            negativeDays = 0;
        }
        if(negativeDays >= 90) {
            stop();
            mainWindowController.handleBankruptcy();
            negativeDays = 0;
        }
    }

    private static void endDay() throws InterruptedException {
        endDaySemaphore.p();
        //Uwaga, zaraz koniec dnia, już nic nie robcie!
        mainSemaphore.p(Config.getSimulationMaximalThreadsCount());
        //SEKCJA KRYTYCZNA
        //Kończę dzien...
        sumUpDay();
        //KONIEC SEKCJI KRYT.
        mainSemaphore.v(Config.getSimulationMaximalThreadsCount());
        //Dobra, zakonczylem dzien
        endDaySemaphore.v();
    }

    public static void stop() {
        if(isRunning == true) {
            isRunning = false;
            mainWindowController.enableButtons();
            VodManager.stopThreads();
        }
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public static boolean isSimulationStarted() {
        return simulationStarted;
    }

    public static boolean isSimulationEnded() {
        return simulationEnded;
    }

    public static SimpleSemaphore getMainSemaphore() {
        return mainSemaphore;
    }

    public static SimpleSemaphore getEndDaySemaphore() {
        return endDaySemaphore;
    }

    public static Date getNow() {
        return now;
    }

    public static Integer getDaysPassed() {
        return daysPassed;
    }

    public static HashMap<SimulatorField, Object> getObjectsToSerialize() {
        HashMap<SimulatorField, Object> objects = new HashMap<>();
        objects.put(SimulatorField.SIMULATION_STARTED, simulationStarted);
        objects.put(SimulatorField.SIMULATION_ENDED, simulationEnded);
        objects.put(SimulatorField.NOW, now);
        objects.put(SimulatorField.DAYS_PASSED, daysPassed);
        objects.put(SimulatorField.NEGATIVE_DAYS, negativeDays);

        return objects;
    }

    public static void loadSerializedObjects(HashMap<SimulatorField, Object> objects) {
        simulationStarted = (boolean) objects.get(SimulatorField.SIMULATION_STARTED);
        simulationEnded = (boolean) objects.get(SimulatorField.SIMULATION_ENDED);
        now = (Date) objects.get(SimulatorField.NOW);
        daysPassed = (int) objects.get(SimulatorField.DAYS_PASSED);
        negativeDays = (int) objects.get(SimulatorField.NEGATIVE_DAYS);
        initialize();
    }
}
