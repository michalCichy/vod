package backend;

import java.io.Serializable;

public enum StreamGenre implements Serializable {
    CONCERT,
    THEATRICAL_PERFORMANCE,
    OTHER
}
