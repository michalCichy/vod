package backend;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Stream extends Product implements Serializable {
    private int duration;
    private LocalTime start;
    private StreamGenre genre;

    public Stream(
            String name,
            Float price,
            String imgPath,
            String description,
            Date worldReleaseDate,
            Date vodReleaseDate,
            int duration,
            Provider provider,
            ArrayList<String> cast,
            String productionCountry,
            LocalTime start,
            StreamGenre genre) {
        super(name, price, imgPath, description, worldReleaseDate, vodReleaseDate, provider, cast, productionCountry);
        this.duration = duration;
        this.start = start;
        this.genre = genre;
    }

    public LocalTime getStart() {
        return start;
    }

    public StreamGenre getGenre() {
        return genre;
    }

    @Override
    public int getDuration() {
        return duration;
    }

    public String getTextInfo() {
        String productInfo = super.getTextInfo();
        return "STREAM " + productInfo +
                " " + Integer.toString(duration) +
                " " + start.toString() +
                " " + (genre == null ? "BRAK" : genre);
    }

    @Override
    public HashMap<String, String> getShowableInfo() {
        HashMap<String, String> info = super.getShowableInfo();
        info.put("Czas trwania", Integer.toString(duration));
        info.put("Godzina startu", start.toString());
        info.put("Gatunek", Product.convertStreamGenreToString(genre));

        return info;
    }
}
