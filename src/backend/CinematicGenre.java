package backend;

import java.io.Serializable;

public enum CinematicGenre implements Serializable {
    ACTION,
    DRAMA,
    COMEDY,
    KIDS,
    DOCUMENTARY,
}
