package backend;

import java.io.Serializable;

public enum Quality implements Serializable {
    DVD_RIP,
    HD720,
    HD1080,
    UHD
}
