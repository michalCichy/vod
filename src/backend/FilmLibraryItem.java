package backend;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class FilmLibraryItem extends LibraryItem implements Serializable {
    private Date expireDate;

    public FilmLibraryItem(Product product, Date purchaseDate) {
        super(product, purchaseDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(purchaseDate);
        calendar.add(Calendar.DATE, Config.getFilmDefaultTimeForWatching());
        this.expireDate = calendar.getTime();
    }

    public Date getExpireDate() {
        return expireDate;
    }
}
