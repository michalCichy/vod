package backend;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Film extends Product implements Serializable {
    private int duration;
    private CinematicGenre genre;
    private String trailerUrl;
    private int timeForWatching;

    public Film(
            String name,
            Float price,
            String imgPath,
            String description,
            Date worldReleaseDate,
            Date vodReleaseDate,
            int duration,
            Provider provider,
            ArrayList<String> cast,
            String productionCountry,
            CinematicGenre genre,
            String trailerUrl,
            int timeForWatching) {
        super(name, price, imgPath, description, worldReleaseDate, vodReleaseDate, provider, cast, productionCountry);
        this.duration = duration;
        this.genre = genre;
        this.trailerUrl = trailerUrl;
        this.timeForWatching = timeForWatching;
    }

    public CinematicGenre getGenre() {
        return genre;
    }

    @Override
    public int getDuration() {
        return duration;
    }

    public String getTextInfo() {
        String productInfo = super.getTextInfo();
        return "FILM " + productInfo +
                " " + Integer.toString(duration) +
                " " + (genre == null ? "BRAK" : genre);
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    @Override
    public HashMap<String, String> getShowableInfo() {
        HashMap<String, String> info = super.getShowableInfo();
        info.put("Czas trwania", Integer.toString(duration));
        info.put("Gatunek", Product.convertCinematicGenreToString(genre));
        info.put("Trailer:", trailerUrl);

        return info;
    }

    @Override
    public LibraryItem getLibraryItem() {
        return new FilmLibraryItem(this, Simulator.getNow());
    }
}
