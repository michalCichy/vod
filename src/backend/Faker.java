package backend;

import backend.exceptions.EmptyCollectionException;
import backend.exceptions.NoProvidersException;

import java.io.File;
import java.time.LocalTime;
import java.util.*;

public class Faker {
    private static ArrayList<String> userNames = new ArrayList<>();
    private static ArrayList<String> userLastNames = new ArrayList<>();

    private static ArrayList<String> companyNameFirstWord = new ArrayList<>();
    private static ArrayList<String> companyNameSecondWord = new ArrayList<>();

    private static ArrayList<String> actors = new ArrayList<>();

    private static ArrayList<String> positivePhrases = new ArrayList<>();
    private static ArrayList<String> negativePhrases = new ArrayList<>();

    private static ArrayList<String> countries = new ArrayList<>();

    private static ArrayList<String> cinematicNameNouns = new ArrayList<>();
    private static ArrayList<String> cinematicNameNounsGenitivum = new ArrayList<>();

    private static ArrayList<String> concertNameNouns = new ArrayList<>();
    private static ArrayList<String> concertNameAdjectives = new ArrayList<>();

    static {
        userNames.addAll(Arrays.asList("Franek", "Darek", "Michał", "Beata", "Barbara",
                "Marta", "Dominika", "Janusz", "Grażyna"));
        userLastNames.addAll(Arrays.asList("Kowalski", "Nowak", "Johnson", "Eastwood",
                "Pitt", "Uzależniony", "Wolny", "Kwiatkowski", "Kinomaniak"));
        companyNameFirstWord.addAll(Arrays.asList("Television", "International", "Universal", "Poznański", "Podlaski", "Serial"));
        companyNameSecondWord.addAll(Arrays.asList("Pictures", "Cinema", "Koncern Filmowy", "King", "Boss", "Movie Corp."));

        actors.addAll(Arrays.asList("Clint Eastwood", "Harrison Ford", "Tom Cruise", "Monica Bellucci",
                "Tom Hardy", "Pamela Anderson", "Jan z Łomży", "Michał Żebrowski", "Bogusław Linda"));

        positivePhrases.addAll(Arrays.asList("Wybitne dzieło.", "Najlepsze, co w życiu widziałem.",
                "Artyści w świetnej formie.", "Wiele lat ciężkiej pracy przyniosło owoce.",
                "Budżet liczony w setkach milionów dolarów.", "Coś wspaniałego!",
                "Takich emocji nie przeżyłem od roku 1969.", "Oglądałem w napięciu od samego początku aż do napisów końcowych."));
        negativePhrases.addAll(Arrays.asList("Takiej szmiry nie widziałem od 1954 roku.",
                "Dno totalne.", "Strata pieniędzy.", "Słabe. Nawet bardzo słabe.",
                "Końcówka mnie zaskoczyła, ale i tak nie warto oglądać.", "Potwornie nudne. I do tego długie."));
        countries.addAll(Arrays.asList("USA", "Polska", "Rumunia", "Szwecja", "Węgry", "Nepal"));

        cinematicNameNouns.addAll(Arrays.asList("Niezwykłe przygody", "Tragedia", "Horror",
                "Prawdziwa Historia", "Życie", "Życie i śmierć", "Porażka", "Blaszany kubek",
                "Czarodziejski flet", "Piękne chwile", "Upadek", "Egzotyczne podróże"));
        cinematicNameNounsGenitivum.addAll(Arrays.asList("Johna Smitha", "Jaśka ze wsi",
                "Władysława z Sochaczewa", "wiejskiego kundelka", "króla Bolesława", "Maryli Rodowicz"));

        concertNameNouns.addAll(Arrays.asList("Queen", "Freddie Mercury", "The Beatles",
                "Metallica", "Ozzy Osbourne", "Deep Purple", "Bee Gees"));

        concertNameAdjectives.addAll(Arrays.asList("symfonicznie", "unplugged", "na Stadionie Narodowym",
                "z udziałem chóru dziewczęcego ze SP nr 8", "- tego jeszcze nie było!"));
    }

    public static String getUserName() {
        String name = userNames.get((int) (Math.random() * userNames.size()));
        String lastName = userLastNames.get((int) (Math.random() * userLastNames.size()));

        return name + " " + lastName;
    }

    private static String getImage(String dir) {
        Integer range = 0;
        String path = "pictures/" + dir +"/";
        File file = new File("src/frontend/" + path + range.toString() + ".png");
        while(file.exists()) {
            range++;
            file = new File("src/frontend/" + path + range.toString() + ".png");
        }
        if (range == 0) {
            return null;
        } else {
            int selectedImageNumber = (int) (Math.random() * range);
            return path + selectedImageNumber + ".png";
        }
    }

    public static String getProviderImage() {
        return getImage("providers");
    }

    public static String getProductImage() {
        return getImage("products");
    }

    public static String getUserImage() {
        return getImage("users");
    }

    public static String getDescription() {
        String description = "";
        ArrayList phrases;
        if(Math.random() > 0.6) {
            phrases = new ArrayList(positivePhrases);
        } else {
            phrases = new ArrayList(negativePhrases);
        }
        for(int i=0; i<3; i++) {
            String phrase = (String) Weglarz.getRandomElement(phrases);
            phrases.remove(phrase);
            description +=  phrase + " ";
        }

        return description;
    }

    public static Date getWorldReleaseDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Simulator.getNow());
        calendar.add(Calendar.DATE, (-1) * (new Random()).nextInt(Config.getMaximalDaysCountSinceWorldReleaseDate()));
        return calendar.getTime();
    }

    public static Date getFutureDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Simulator.getNow());
        calendar.add(Calendar.DATE, (new Random()).nextInt(Config.getMaximalDaysCountUntilFutureDate()));
        return calendar.getTime();
    }

    public static ArrayList<String> getCast() {
        ArrayList<String> actorsNames = new ArrayList<>(actors);
        ArrayList<String> cast = new ArrayList<>();
        for(int i=0; i<3; i++) {
            String actor = (String) Weglarz.getRandomElement(actors);
            actorsNames.remove(actor);
            cast.add(actor);
        }
        return cast;
    }

    public static String getProductionCountry() {
        if(Math.random() < 0.7) {
            return "USA";
        } else {
            return (String) Weglarz.getRandomElement(countries);
        }
    }

    public static String getCinematicName() {
        return (String) Weglarz.getRandomElement(cinematicNameNouns) + " " +
                (String) Weglarz.getRandomElement(cinematicNameNounsGenitivum);
    }

    public static CinematicGenre getCinematicGenre() {
        return (CinematicGenre) Weglarz.getRandomElement(Arrays.asList(CinematicGenre.values()));
    }

    public static StreamGenre getStreamGenre() {
        return (StreamGenre) Weglarz.getRandomElement(Arrays.asList(StreamGenre.values()));
    }

    public static Series getSeries(Provider provider) {
        return new Series(getCinematicName(), Config.getSeriesDefaultPrice(), getProductImage(),
                getDescription(), getWorldReleaseDate(), Simulator.getNow(), provider, getCast(),
                getProductionCountry(), getCinematicGenre());
    }

    public static Series getSeries() {
        Series series = null;
        try {
            series = getSeries((Provider) Weglarz.getRandomElement(VodManager.getProviders()));
        } catch(EmptyCollectionException exception) {
            throw new NoProvidersException("NoProvidersException");
        }
        return series;
    }

    public static String getUrl() {
        Integer nr = (int) (Math.random() * 1000000);
        return "www.trailery.com/" + nr.toString();
    }

    public static Film getFilm(Provider provider) {
        int duration = (int) (Math.random() * Config.getMaximalSingleProductDuration());
        return new Film(getCinematicName(), Config.getFilmDefaultPrice(), getProductImage(),
                getDescription(), getWorldReleaseDate(), Simulator.getNow(), duration, provider,
                getCast(), getProductionCountry(), getCinematicGenre(), getUrl(), Config.getFilmDefaultTimeForWatching());
    }

    public static Film getFilm() {
        Film film = null;
        try {
            film = getFilm((Provider) Weglarz.getRandomElement(VodManager.getProviders()));
        } catch(EmptyCollectionException exception) {
            throw new NoProvidersException();
        }
        return film;
}

    public static Stream getStream(Provider provider) {
        String name = "";
        StreamGenre genre = null;
        int duration = (int) (Math.random() * 300);
        if(Math.random() > 0.6) {
            name = (String) Weglarz.getRandomElement(concertNameNouns) + " " +
                    (String) Weglarz.getRandomElement(concertNameAdjectives);
            genre = StreamGenre.CONCERT;
        } else {
            name = "STREAM" + Integer.toString((int)(Math.random()*100000));
            genre = getStreamGenre();
        }
        Date releaseDate = getFutureDate();

        return new Stream(name, Config.getStreamDefaultPrice(), getProductImage(), getDescription(),
                releaseDate, releaseDate, duration, provider, getCast(), getProductionCountry(),
                LocalTime.of((int) (Math.random() * 24), (int) (Math.random() * 60)), getStreamGenre());
    }

    public static Stream getStream() {
        Provider provider = null;
        try {
            provider = (Provider) Weglarz.getRandomElement(VodManager.getProviders());
        } catch(EmptyCollectionException exception) {
            throw new NoProvidersException();
        }
        return getStream(provider);
    }

    public static String getEmail(String name) {
        String[] splitted = name.split(" ");
        return splitted[0] + Integer.toString((int) (Math.random() * 10000)) + "@" + splitted[1] + ".com";
    }

    public static String getPaymentCardNumber() {
        String paymentCardNumber = Integer.toString((int) (Math.random() * 100000)) +
                Integer.toString((int) (Math.random() * 100000));
        return paymentCardNumber;
    }

    public static User getUser() {
        String pesel = "9" + Integer.toString((int) (Math.random() * 100000)) +
                Integer.toString((int) (Math.random() * 100000));
        String name = getUserName();

        return new User(pesel, getUserImage(), name, getEmail(name), new Date(), getPaymentCardNumber(), null);
    }

    public static String getProviderName() {
        return (String) Weglarz.getRandomElement(companyNameFirstWord) + " " +
                (String) Weglarz.getRandomElement(companyNameSecondWord);
    }

    public static Contract getContract() {
        ContractType contractType = (ContractType) Weglarz.getRandomElement(Arrays.asList(ContractType.values()));
        float amount = (new Random()).nextFloat() *
                Config.getMaximalContractProfitPerView() + Config.getMinimalContractProfitPerView();
        if (contractType == ContractType.PER_MONTH) {
            amount = amount * (VodManager.getUsers().size() + 1);
        }
        return new Contract(amount, contractType);
    }

    public static Provider getProvider() {
        return new Provider(getProviderName(), getProviderImage(), getContract());
    }
}
