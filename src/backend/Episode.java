package backend;

import java.io.Serializable;
import java.util.Date;

public class Episode implements Serializable {
    private Date releaseDate;
    private int duration;

    public Episode(Date releaseDate, int duration) {
        this.releaseDate = releaseDate;
        this.duration = duration;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public int getDuration() {
        return duration;
    }
}
