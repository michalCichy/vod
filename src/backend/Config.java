package backend;

import java.util.HashMap;

public class Config implements StaticSerializable {
    private static String serializedDataPath = "serialized";

    private static Float seriesDefaultPrice = 10f;
    private static Float filmDefaultPrice = 10f;
    private static Float streamDefaultPrice = 10f;

    private static Float defaultBasicPrice = 40f;
    private static Float defaultFamilyPrice = 100f;
    private static Float defaultPremiumPrice = 120f;


    private static Integer filmDefaultTimeForWatching = 10;

    private static Integer initialSeriesCount = 3;
    private static Integer initialFilmsCount = 3;
    private static Integer initialStreamsCount = 3;
    private static Integer initialProvidersCount = 3;
    private static Integer initialUsersCount = 3;

    private static long simulationDayDuration = 500;
    private static int simulationMaximalThreadsCount = 1000;

    private static int userMaximalSleepTime = 400;
    private static int providerMaximalSleepTime = 400;
    private static boolean runnableItemLogEnabled = false;

    private static int maximalDaysCountSinceWorldReleaseDate = 300;
    private static int maximalDaysCountUntilFutureDate = 100;

    private static float maximalContractProfitPerView = 10;
    private static float minimalContractProfitPerView = 5;

    private static double userBuyingLicenseFrequency = 0.01;
    private static double userBuyingProductsFrequency = 0.3;
    private static double userWatchingProductFrequency = 0.3;

    private static int maximalSingleProductDuration = 300;

    private static double providerNewEpisodeReleasingFrequency = 0.5;
    private static double providerNewProductReleasingFrequency = 0.5;
    private static double providerRenegotiationFrequency = 0.1;

    private static boolean vodLogEnabled = true;

    public static Float getFilmDefaultPrice() {
        return filmDefaultPrice;
    }

    public static Float getSeriesDefaultPrice() {
        return seriesDefaultPrice;
    }

    public static Float getStreamDefaultPrice() {
        return streamDefaultPrice;
    }

    public static Integer getFilmDefaultTimeForWatching() {
        return filmDefaultTimeForWatching;
    }

    public static Integer getInitialSeriesCount() {
        return initialSeriesCount;
    }

    public static Integer getInitialFilmsCount() {
        return initialFilmsCount;
    }

    public static Integer getInitialStreamsCount() {
        return initialStreamsCount;
    }

    public static Integer getInitialProvidersCount() {
        return initialProvidersCount;
    }

    public static Integer getInitialUsersCount() {
        return initialUsersCount;
    }

    public static long getSimulationDayDuration() {
        return simulationDayDuration;
    }

    public static int getSimulationMaximalThreadsCount() {
        return simulationMaximalThreadsCount;
    }

    public static int getUserMaximalSleepTime() {
        return userMaximalSleepTime;
    }

    public static boolean isRunnableItemLogEnabled() {
        return runnableItemLogEnabled;
    }

    public static int getMaximalDaysCountSinceWorldReleaseDate() {
        return maximalDaysCountSinceWorldReleaseDate;
    }

    public static int getMaximalDaysCountUntilFutureDate() {
        return maximalDaysCountUntilFutureDate;
    }

    public static float getMaximalContractProfitPerView() {
        return maximalContractProfitPerView;
    }

    public static float getMinimalContractProfitPerView() {
        return minimalContractProfitPerView;
    }

    public static double getUserBuyingLicenseFrequency() {
        return userBuyingLicenseFrequency;
    }

    public static double getUserBuyingProductsFrequency() {
        return userBuyingProductsFrequency;
    }

    public static double getUserWatchingProductFrequency() {
        return userWatchingProductFrequency;
    }

    public static int getMaximalSingleProductDuration() {
        return maximalSingleProductDuration;
    }

    public static double getProviderNewEpisodeReleasingFrequency() {
        return providerNewEpisodeReleasingFrequency;
    }

    public static double getProviderNewProductReleasingFrequency() {
        return providerNewProductReleasingFrequency;
    }

    public static int getProviderMaximalSleepTime() {
        return providerMaximalSleepTime;
    }

    public static boolean isVodLogEnabled() {
        return vodLogEnabled;
    }

    public static double getProviderRenegotiationFrequency() {
        return providerRenegotiationFrequency;
    }

    public static Float getDefaultBasicPrice() {
        return defaultBasicPrice;
    }

    public static Float getDefaultFamilyPrice() {
        return defaultFamilyPrice;
    }

    public static Float getDefaultPremiumPrice() {
        return defaultPremiumPrice;
    }

    public static void setFilmDefaultPrice(Float filmDefaultPrice) {
        Config.filmDefaultPrice = filmDefaultPrice;
    }

    public static void setSeriesDefaultPrice(Float seriesDefaultPrice) {
        Config.seriesDefaultPrice = seriesDefaultPrice;
    }

    public static void setStreamDefaultPrice(Float streamDefaultPrice) {
        Config.streamDefaultPrice = streamDefaultPrice;
    }

    public static void setDefaultBasicPrice(Float defaultBasicPrice) {
        Config.defaultBasicPrice = defaultBasicPrice;
    }

    public static void setDefaultFamilyPrice(Float defaultFamilyPrice) {
        Config.defaultFamilyPrice = defaultFamilyPrice;
    }

    public static void setDefaultPremiumPrice(Float defaultPremiumPrice) {
        Config.defaultPremiumPrice = defaultPremiumPrice;
    }

    public static String getSerializedDataPath() {
        return serializedDataPath;
    }

    public static HashMap<ConfigField, Object> getObjectsToSerialize() {
        HashMap<ConfigField, Object> objects = new HashMap<>();

        objects.put(ConfigField.FILM_DEFAULT_PRICE, filmDefaultPrice);
        objects.put(ConfigField.SERIES_DEFAULT_PRICE, seriesDefaultPrice);
        objects.put(ConfigField.STREAM_DEFAULT_PRICE, streamDefaultPrice);
        objects.put(ConfigField.DEFAULT_BASIC_PRICE, defaultBasicPrice);
        objects.put(ConfigField.DEFAULT_FAMILY_PRICE, defaultFamilyPrice);
        objects.put(ConfigField.DEFAULT_PREMIUM_PRICE, defaultPremiumPrice);

        return objects;
    }

    public static void loadSerializedObjects(HashMap<ConfigField, Object> objects) {
        filmDefaultPrice = (Float) objects.get(ConfigField.FILM_DEFAULT_PRICE);
        seriesDefaultPrice = (Float) objects.get(ConfigField.SERIES_DEFAULT_PRICE);
        streamDefaultPrice = (Float) objects.get(ConfigField.STREAM_DEFAULT_PRICE);
        defaultBasicPrice = (Float) objects.get(ConfigField.DEFAULT_BASIC_PRICE);
        defaultFamilyPrice = (Float) objects.get(ConfigField.DEFAULT_FAMILY_PRICE);
        defaultPremiumPrice = (Float) objects.get(ConfigField.DEFAULT_PREMIUM_PRICE);
    }
}
