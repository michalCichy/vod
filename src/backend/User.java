package backend;

import backend.exceptions.EmptyCollectionException;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Thread.sleep;

public class User extends RunnableItem implements Serializable {
    private String pesel;
    private String imgPath;
    private String name;
    private String email;
    private Date birthDate;
    private String paymentCardNumber;
    private License license;
    private ArrayList<LibraryItem> library;
    private int dayWhenLastPaidForLicense = 0;

    public User(
            String pesel,
            String imgPath,
            String name,
            String email,
            Date birthDate,
            String paymentCardNumber,
            License license) {
        super(Config.getUserMaximalSleepTime());
        this.pesel = pesel;
        this.imgPath = imgPath;
        this.name = name;
        this.email = email;
        this.birthDate = birthDate;
        this.paymentCardNumber = paymentCardNumber;
        this.license = license;
        this.library = new ArrayList<>();
    }

    public String getPesel() {
        return pesel;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPaymentCardNumber() {
        return paymentCardNumber;
    }

    public License getLicense() {
        return license;
    }

    public ArrayList<LibraryItem> getLibrary() {
        return library;
    }

    @Override
    public HashMap<String, String> getShowableInfo() {
        HashMap<String, String> info = new LinkedHashMap<>();
        info.put("Imię i nazwisko", name);
        info.put("PESEL", pesel);
        info.put("email", email);
        info.put("data urodzenia", birthDate.toString());
        info.put("nr karty kredytowej", paymentCardNumber);
        if (license == null) {
            info.put("rodzaj abonamentu", "brak");
        } else {
            info.put("rodzaj abonamentu", license.getName());
        }
        return info;
    }

    @Override
    public String getImgPath() {
        return imgPath;
    }

    @Override
    public boolean matches(String searchText) {
        return name.toLowerCase().contains(searchText.toLowerCase());
    }

    @Override
    protected void performRoutine() {
        logger.log("Dzialam... " + getName());

        if (Weglarz.probability(Config.getUserBuyingLicenseFrequency())) {
            buyLicense();
        }

        ArrayList<LibraryItem> newlyAcquired = null;

        if (Weglarz.probability(Config.getUserBuyingProductsFrequency())) {
            float cost = 0;
            newlyAcquired = acquireProducts();
            for (LibraryItem libraryItem : newlyAcquired) {
                cost += libraryItem.getProduct().getPrice();
            }
            VodManager.addToBudget(cost);
        }

        if (newlyAcquired != null && newlyAcquired.size() > 0) {
            watch((LibraryItem) Weglarz.getRandomElement(newlyAcquired));
        }

        for (LibraryItem libraryItem : library) {
            if (libraryItem.getProduct() instanceof Stream) {
                if (Simulator.getNow().compareTo(libraryItem.getProduct().getVodReleaseDate()) == 0) {
                    watch(libraryItem);
                }
            }
        }

        if(license != null) {
            if(Simulator.getDaysPassed() - dayWhenLastPaidForLicense > 30) {
                VodManager.addToBudget(license.getPrice());
                dayWhenLastPaidForLicense = Simulator.getDaysPassed();
            }
        }
    }

    private void buyLicense() {
        if (license == null) {
            if (Weglarz.probability(0.5)) {
                license = VodManager.getLicenses().get("basic");
            } else if (Weglarz.probability(0.4)) {
                license = VodManager.getLicenses().get("family");
            } else {
                license = VodManager.getLicenses().get("premium");
            }
        }
    }

    private ArrayList<LibraryItem> acquireProducts() {
        ArrayList<LibraryItem> libraryItems = new ArrayList<>();
        ArrayList<LibraryItem> trolley = new ArrayList<>();
        try {
            for (int i = 0; i < 5; i++) {
                Product product = (Product) Weglarz.getRandomElement(VodManager.getProducts());
                if (!library.stream().anyMatch(e -> e.getProduct() == product)) {
                    if (product instanceof Series) {
                        Series series = (Series) product;
                        if (series.getEpisodesCount() > 0) {
                            LibraryItem libraryItem = product.getLibraryItem();
                            library.add(libraryItem);
                            trolley.add(libraryItem);
                        }
                    } else {
                        LibraryItem libraryItem = product.getLibraryItem();
                        library.add(libraryItem);
                        trolley.add(libraryItem);
                    }
                } else if (product instanceof Series) {
                    SeriesLibraryItem seriesLibraryItem = (SeriesLibraryItem) library.stream().filter(e -> e.getProduct() == product).
                            collect(Collectors.toList()).get(0);
                    if (((Series) (seriesLibraryItem.getProduct())).getEpisodesCount() >
                            seriesLibraryItem.getPossessedEpisodesCount()) {
                        seriesLibraryItem.addEpisode();
                        trolley.add(seriesLibraryItem);
                    }
                }
            }
        } catch (EmptyCollectionException e) {
            System.out.println(e.getMessage());
        }

        return trolley;
    }

    private void watch(LibraryItem libraryItem) {
        if (!(libraryItem.getProduct() instanceof Stream) || libraryItem.getProduct().getVodReleaseDate().compareTo(Simulator.getNow()) < 0) {
            if (libraryItem instanceof SeriesLibraryItem) {
                if (((SeriesLibraryItem) libraryItem).unwatchedEpisodeExists()) {
                    double vote = Math.random() * 10;
                    libraryItem.registerWatching(vote);
                }
            } else {
                if (libraryItem.wasWatched() == false) {
                    double vote = Math.random() * 10;
                    libraryItem.registerWatching(vote);
                }
            }
        }
    }

    public void refreshLibrary(Product product) {
        int i=0;
        while(i<library.size()) {
            if(library.get(i).getProduct() == product) {
                library.remove(i);
            } else {
                i++;
            }
        }
    }
}
