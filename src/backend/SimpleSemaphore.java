package backend;

import java.util.concurrent.Semaphore;

public class SimpleSemaphore extends Semaphore {
    SimpleSemaphore(int permits) {
        super(permits, true);
    }

    public void p(int permits) throws InterruptedException {
        this.acquire(permits);
    }
    public void p() throws InterruptedException {
        p(1);
    }

    public void v(int permits) {
        this.release(permits);
    }

    public void v() {
        v(1);
    }
}
