package backend;

import backend.exceptions.EmptyCollectionException;

import java.util.List;

import static java.lang.Thread.sleep;

public class Weglarz {
    public static Object getRandomElement(List list) {
        if(list.size() == 0) {
            throw new EmptyCollectionException("PUSTA KOLEKCJA!");
        }
        return list.get((int) (Math.random() * list.size()));
    }

    public static boolean probability(double probability) {
        return Math.random() < probability;
    }
}
