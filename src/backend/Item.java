package backend;

import java.io.Serializable;
import java.util.HashMap;

public abstract class Item implements Serializable {
    public abstract String getName();
    public abstract HashMap<String, String> getShowableInfo();
    public abstract String getImgPath();
    public abstract void handleRemoval();
    public abstract boolean matches(String searchText);
}
