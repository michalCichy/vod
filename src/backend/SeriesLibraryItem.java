package backend;

import java.io.Serializable;
import java.util.Date;

public class SeriesLibraryItem extends LibraryItem implements Serializable {
    int possessedEpisodes = 1;
    int watchedEpisodes = 0;
    public SeriesLibraryItem(Product product, Date purchaseDate) {
        super(product, purchaseDate);
    }

    public void addEpisode() {
        possessedEpisodes++;
    }

    @Override
    public void registerWatching() {
        super.registerWatching();
        watchedEpisodes++;
    }

    public boolean unwatchedEpisodeExists() {
        return possessedEpisodes > watchedEpisodes;
    }

    public int getPossessedEpisodesCount() {
        return possessedEpisodes;
    }
}
