package backend;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static backend.VodManagerField.*;

public class VodManager implements StaticSerializable {
    private static List<Series> series;
    private static List<Film> films;
    private static List<Stream> streams;
    private static List<User> users;
    private static List<Provider> providers;
    private static volatile Float budget;
    private static HashMap<String, License> licenses;
    private static Logger logger = new Logger(Config.isVodLogEnabled());

    static {
        series = Collections.synchronizedList(new ArrayList<Series>());
        films = Collections.synchronizedList(new ArrayList<Film>());
        streams = Collections.synchronizedList(new ArrayList<Stream>());
        users = Collections.synchronizedList(new ArrayList<User>());
        providers = Collections.synchronizedList(new ArrayList<Provider>());
        budget = 0f;

        licenses = new HashMap<>();
        licenses.put("basic", new License("Basic", Config.getDefaultBasicPrice(), 1, Quality.HD720));
        licenses.put("family", new License("Family", Config.getDefaultFamilyPrice(), 5, Quality.HD1080));
        licenses.put("premium", new License("Premium", Config.getDefaultPremiumPrice(), 10, Quality.UHD));
    }

    private static void seed() {
        for(int i=0; i<Config.getInitialProvidersCount(); i++) {
            addProvider(Faker.getProvider());
        }
        for(int i=0; i<Config.getInitialSeriesCount(); i++) {
            addProduct(Faker.getSeries());
        }
        for(int i=0; i<Config.getInitialFilmsCount(); i++) {
            addProduct(Faker.getFilm());
        }
        for(int i=0; i<Config.getInitialStreamsCount(); i++) {
            addProduct(Faker.getStream());
        }
        for(int i=0; i<Config.getInitialUsersCount(); i++) {
            addUser(Faker.getUser());
        }
    }

    public static void initialize() {seed();}

    public static HashMap<VodManagerField, Object> getObjectsToSerialize() {
        HashMap objects = new HashMap<VodManagerField, Object>();
        objects.put(SERIES, series);
        objects.put(FILMS, films);
        objects.put(STREAMS, streams);
        objects.put(USERS, users);
        objects.put(PROVIDERS, providers);
        objects.put(BUDGET, budget);
        objects.put(LICENSES, licenses);

        return objects;
    }

    public static void loadSerializedObjects(HashMap<VodManagerField, Object> objects) {
        series = (List) objects.get(SERIES);
        films = (List) objects.get(FILMS);
        streams = (List) objects.get(STREAMS);
        users = (List) objects.get(USERS);
        providers = (List) objects.get(PROVIDERS);
        budget = (Float) objects.get(BUDGET);
        licenses = (HashMap) objects.get(LICENSES);
    }

    public static void startThreads() {
        for(User user : users) {
            user.startThread();
        }

        for(Provider provider : providers) {
            provider.startThread();
        }
    }

    public static void stopThreads() {
        for(User user : users) {
            user.stopThread();
        }

        for(Provider provider : providers) {
            provider.stopThread();
        }
    }

    public static void addUser(User user) {
        users.add(user);
        if(Simulator.isSimulationStarted()) {
            user.startThread();
        }
    }

    public static void addProvider(Provider provider) {
        providers.add(provider);
        if(Simulator.isSimulationStarted()) {
            provider.startThread();
        }
    }

    public static void addProduct(Product product) {
        if(product != null) {
            if(product instanceof Series) {
                series.add((Series) product);
            } else if(product instanceof  Film) {
                films.add((Film) product);
            } else if(product instanceof Stream) {
                streams.add((Stream) product);
            }
        }
    }

    public static List<Product> getProducts() {
            List<Product> products = Collections.synchronizedList(new ArrayList<>());
            products.addAll(series);
            products.addAll(films);
            products.addAll(streams);

            return products;
    }

    public static List<Series> getSeries() {
        return series;
    }
    public static List<Film> getFilms() {
        return films;
    }
    public static List<Stream> getStreams() {
        return streams;
    }

    public static List<User> getUsers() {
        return users;
    }

    public static List<Provider> getProviders() {
        return providers;
    }

    public static float getBudget() {
        return budget;
    }

    public static HashMap<String, License> getLicenses() {
        return licenses;
    }

    public synchronized static void addToBudget(float sum) {
        budget += sum;
        if(sum > 0) {
            logger.log("PLUS " + sum);
        }
    }

    public static synchronized void removeItem(Item item) {
        if(item instanceof Series) {
            series.remove(item);
            refreshUsersLibraries((Series) item);
        } else if(item instanceof Film) {
            films.remove(item);
            refreshUsersLibraries((Film) item);
        } else if(item instanceof Stream) {
            streams.remove((Stream) item);
            refreshUsersLibraries((Stream) item);
        } else if(item instanceof User) {
            users.remove(item);
        } else if(item  instanceof Provider) {
            providers.remove(item);
        }
    }

    private static void refreshUsersLibraries(Product product) {
        for(User user : users) {
            user.refreshLibrary(product);
        }
    }

    static void registerNewDay() {
        for(Product product : getProducts()) {
            product.registerNewDay();
        }
    }

    public static void manageMonthlyBudget() {
        for(Provider provider : providers) {
            if (provider.getContract().getType() == ContractType.PER_MONTH) {
                float amount = provider.getContract().getProfit();
                budget -= amount;
                logger.log("MINUS " + Float.toString(amount) +
                        " ->" + provider.getName());
            }
        }
    }

    public static void manageDailyBudget() {
        List dailyPaidProviders = providers.stream()
                .filter(p -> {return p.getContract().getType() == ContractType.PER_VIEW;})
                .collect(Collectors.toList());
        for(Product product : getProducts()) {
            if(dailyPaidProviders.contains(product.getProvider())) {
                float amount = product.getRecentViews() * product.getProvider().getContract().getProfit();
                budget -= amount;
                if(amount > 0) {
                    logger.log("MINUS " +
                            Float.toString(amount) + " ->" + product.getProvider().getName());
                }
            }
        }
    }

    public static boolean negativeBudget() {
        return budget < 0;
    }

    public static void updateLicenses() {
        licenses.get("basic").setPrice(Config.getDefaultBasicPrice());
        licenses.get("family").setPrice(Config.getDefaultFamilyPrice());
        licenses.get("premium").setPrice(Config.getDefaultPremiumPrice());
    }
 }
