package backend;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 */
public class Contract implements Serializable {
    private float profit;
    private ContractType type;
    public static HashMap<ContractType, String> contractTypeToString = new HashMap<>();

    static {
        contractTypeToString.put(ContractType.PER_MONTH, "miesięcznie");
        contractTypeToString.put(ContractType.PER_VIEW, "za każde obejrzenie");
    }

    public Contract(float profit, ContractType type) {
        this.profit = profit;
        this.type = type;
    }

    public float getProfit() {
        return profit;
    }
    public ContractType getType() {
        return type;
    }

}
