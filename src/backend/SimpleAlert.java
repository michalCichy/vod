package backend;

import javafx.scene.control.Alert;

public class SimpleAlert {
    public SimpleAlert(String s) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("UWAGA!");
        alert.setHeaderText(null);
        alert.setContentText(s);
        alert.showAndWait();
    }
}
