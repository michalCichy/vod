package backend;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class Provider extends RunnableItem implements Serializable {
    private String name;
    private String imgPath;
    private Contract contract;

    public Provider(String name, String imgPath, Contract contract) {
        super(Config.getProviderMaximalSleepTime());
        this.name = name;
        this.imgPath = imgPath;
        this.contract = contract;
    }

    public String getName() {
        return name;
    }

    public Contract getContract() {
        return contract;
    }

    public Product releaseProduct() {
        return null;
    }

    public void renegotiateContract() {
        contract = Faker.getContract();
    }

    @Override
    public HashMap<String, String> getShowableInfo() {
        HashMap<String, String> info = new LinkedHashMap<>();
        info.put("nazwa", name);
        info.put("zysk", Float.toString(contract.getProfit()));
        info.put("rodzaj umowy", Contract.contractTypeToString.get(contract.getType()));
        return info;
    }

    @Override
    public String getImgPath() {
        return imgPath;
    }

    @Override
    public boolean matches(String searchText) {
        return name.toLowerCase().contains(searchText.toLowerCase());
    }

    private void addNewSeriesEpisode() {
        Series series = (Series) Weglarz.getRandomElement(VodManager.getSeries());
        series.addEpisode();
    }

    private void addNewSeries() {
        VodManager.addProduct(Faker.getSeries(this));
    }

    private void addNewFilm() {
        VodManager.addProduct(Faker.getFilm(this));
    }

    private void addNewStream() {
        VodManager.addProduct(Faker.getStream(this));
    }

    @Override
    protected void performRoutine() {
        logger.log("Działam... " + getName());
        if(Weglarz.probability(Config.getProviderNewEpisodeReleasingFrequency())) {
            synchronized (VodManager.getSeries()) {
                for(Product product : VodManager.getSeries()) {
                    Series series = (Series) product;
                    if(series.getProvider() == this) {
                        series.addEpisode();
                    }
                }
            }
        }
        if(VodManager.getProducts().size() < VodManager.getUsers().size()/2 &&
                Weglarz.probability(Config.getProviderNewProductReleasingFrequency())) {
            for (int i = 0; i < VodManager.getUsers().size() /2 + 1 ; i++) {
                if(Weglarz.probability(0.3)) {
                    addNewSeries();
                } else if(Weglarz.probability(0.3)) {
                    addNewFilm();
                } else {
                    addNewStream();
                }
            }
        }

        if(Weglarz.probability(Config.getProviderRenegotiationFrequency())) {
            renegotiateContract();
        }
    }
}
